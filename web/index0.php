<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
require_once '/../app/Controller.php';

$map = array(
    'login' => array('controller' =>'Controller', 'action' =>'login'),
    'clientes' => array('controller' =>'Controller', 'action' =>'menuClientes'),
    'menuMain'=>array('controller' =>'Controller', 'action' =>'menuMain'),
);
if (isset($_GET['ctl'])){
    if (isset($map[$_GET['ctl']])){
        $ruta=$_GET['ctl'];
    }else {
        header('Status: 404 Not Found');
        echo '<html><body><h1>Error 404: No existe la ruta <i>';
        exit;
    }
}else {
    $ruta='login';
}
$controlador = $map[$ruta];

/*   
if (!isset($_SESSION)){
    session_start();
    if (!$_SESSION['activa']){
       header("Location:index.php?ctl=login");
       exit;
  }
}
*/
$existe= (method_exists($controlador['controller'],$controlador['action']));
$tengo= [$controlador['controller'],$controlador['action']];
if (method_exists($controlador['controller'],$controlador['action'])){
    call_user_function(array (new $controlador['controller'],$controlador['action']));
}else{
    call_user_function(array (new $controlador['controller'],$controlador['action']));
    //header('Status: 404 Not Found');
}
?>