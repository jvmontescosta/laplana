<?php
session_start();
// web/index.php
// carga del modelo y los controladores
require_once __DIR__ . '/../app/Config.php';
require_once __DIR__ . '/../app/modelo/ModeloInterface.php';
require_once __DIR__ . '/../app/Controller.php';
// enrutamiento
$map =[
'login' => array('controller' =>'Controller' , 'action' =>'login'),
'menuMain' => array('controller' =>'Controller','action' =>'menuMain'),
'inicio' => array('controller' =>'Controller', 'action' =>'menuMain'),
'menuClientes' => array('controller' =>'Controller', 'action' =>'menuClientes'),
'menuUnCliente' => array('controller' =>'Controller', 'action' =>'menuUnCliente'),
'insertar' => array('controller' =>'Controller', 'action' =>'insertar'),
'buscar' => array('controller' =>'Controller', 'action' =>'buscarPorNombre'),
'menuRepresentantes' => array('controller' =>'Controller','action' =>'menuRepresentantes'),
'altaRepresentantes' => array('controller' =>'Controller','action' =>'altaRepresentantes'),
'grabarRepresentantes' => array('controler' =>'Controller','action' =>'grabarRepresentantes'),
'verRepre' => array('controller' =>'Controller', 'action' =>'verRepre'),
'menuClientes' => array('controller' =>'Controller','action' =>'menuClientes'),
'altaClientes' =>array('controller'=>'Controller','action' =>'altaClientes'),
'verPresupuestosCliente' => array('controller' => 'Controller','action' =>'verPresupuestosCliente'),
'cerrarSesion' => array('controller' => 'Controller','action' =>'cerrarSesion'),
'instalacion' => array('controller' => 'Controller','action' =>'instalacion'),
'contacto' => array('controller' => 'Controller','action' =>'contacto'),
'verUnArticulo' =>array('controller' =>'Controller','action'=>'verUnArticulo'),
'añadirArticulosCliente'=>array('controller' =>'Controller','action'=>'añadirArticulosCliente'),
'añadirPresupuestoCliente'=>array('controller' =>'Controller','action'=>'añadirPresupuestoCliente'),
'añadirVisitaCliente'=>array('controller' =>'Controller','action'=>'añadirVisitaCliente'),
'grabarArticulo'=> array('controller' =>'Controller','action'=>'grabarArticulo'),
'grabarPresupuesto'=>array('controller'=>'Controller','action'=>'grabarPresupuesto'),
'verUnPresupuesto'=>array('controller'=>'Controller','action'=>'verUnPresupuesto'),
'recargaCliente'=> array('controller' =>'Controller','action'=>'recargaCliente') ,
];

// Parseo de la ruta
if (isset($_GET['ctl'])) {
	if (isset($map[$_GET['ctl']])) {
		$ruta = $_GET['ctl'];
	} else {
			//Si la opción seleccionada no existe en el array de mapeo, mostramos pantalla de error
			header('Status: 404 Not Found');
			echo '<html><body><h1>Error 404: No existe la ruta <i>' .
			$_GET['ctl'] .'</p></body></html>';
			exit;
			}
	} else {
		//Si no se ha seleccionado nada mostraremos pantalla de login
		$ruta = 'login';
}

$controlador = $map[$ruta];

if(!isset($_SESSION["nombre"])){
      
     // $_SESSION['activa']=0;
}    
if (!$_SESSION){
     $ruta = 'login';
     $controlador = $map[$ruta];
}
      
      if (method_exists($controlador['controller'],$controlador['action'])) {
	call_user_func(array(new $controlador['controller'], $controlador['action']));
	} else {
		//Si no existe controlador asociado a la acción, mostramos pantalla de error
		header('Status: 404 Not Found');
		echo '<html><body><h1>Error 404: El controlador <i>' .
		$controlador['controller'] .'->' .	$controlador['action'] .'</i> no existe</h1></body></html>';
        }

?>

  