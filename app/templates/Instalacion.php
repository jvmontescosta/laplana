<?php

include_once 'Modelo.php';
include_once 'funciones.php';
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

            cabecera();

            $modelo = ObtenerModelo();
            $modelo->instalar();
            pie();

?>
    </body>
</html>