<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<html>
    <head>
        <title><?php echo $params['mensaje'] ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo '../web/css/'.Config::$css ?>" />
        <title>Crear y asignar articulo a cliente</title>
    </head>
    <body>
          <div>
             <table id="datosclientes" border='1' width='1000'>
                 <td>Código Cliente</td><td>Razón Social</td><td>Dirección</td><td>Población</td>
                 <td>Provincia</td><td>Codigo Postal</td><td>Teléfono</td><td>Contacto</td>
                 <br>
                  <h3>
                      
             <?php $clientes=$params['cliente'] ; ?>
            <?php foreach ($clientes as $cliente) : ?>
                 <tr>
                    <td><?php echo $cliente->getId() ?> </td>  
                    <td><?php echo $cliente->getRazonSocial() ?> </td>
                    <td><?php echo $cliente->getDireccion() ?> </td>
                    <td><?php echo $cliente->getPoblacion() ?> </td>
                    <td><?php echo $cliente->getProvincia() ?> </td>
                    <td><?php echo $cliente->getCodigoPostal() ?> </td>
                    <td><?php echo $cliente->getTelefono() ?> </td>
                    <td><?php echo $cliente->getContacto() ?> </td>
                   <br>
                 </tr>
             <?php endforeach ?>
                  </h3>
          
             </table>
            <br>
            
        </div>
        <form name="articuloNuevo" action="index.php?ctl=grabarArticulo" method="post">
        <div id="datosarticuloNuevo" >
            Codigo Cliente <input type="text" name="codcliente" value=<?php echo $cliente->getId() ?> disabled="true" size='3'>
            <input type="hidden" name="codigocliente" value=<?php echo $cliente->getId() ?> >
            Codigo <input type="text" name="codigo" autofocus="true" maxlength='6' size="8">
            Tipo de Caja <select name="tiposcaja">
                 <?php $tipocajas=$params['tipocaja'] ; ?>
                <?php foreach ($tipocajas as $tipocaja) : ?>
                        <?php $idcaja = $tipocaja->getId(); ?>
                        <?php $descCaja = $tipocaja->getDescripcion(); 
                       echo '<option value="'.$idcaja.'">'.$descCaja.'</option>';
                      endforeach ?>
            </select>
            Descripcion <input type="text" name="descripcion" maxlength='100' size='100'><br><br>
            Largo <input type="text" name="largo" maxlength='4' size="4">
            Ancho <input type="text" name="ancho" maxlength='4' size="4">
            Alto <input type="text" name="alto" maxlength='4' size="4">
               <?php $tipocalidades=$params['calidades'] ; ?> 
             Calidad<select name="calidad">
                <?php foreach ($tipocalidades as $tipocalidad) : 
                       echo '<option value="'.$tipocalidad->getCalidad().'">'.$tipocalidad->getDescripcion().'</option>';
                      endforeach ?>
                </select>
            Precio <input type="text" name="precio" maxlength='8' size="6">
         </div>
            <div id="botones">
                <input type="submit" name="Validar" value="Validar">
                <input type="submit" name="Cancelar" value="Cancelar">
            </div>
        </form>
    </body>
</html>
