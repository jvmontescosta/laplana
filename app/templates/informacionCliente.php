
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $params['mensaje'] ?></title>
        <!--<link rel="stylesheet" type="text/css" href="<php echo '../web/css/'.Config::$css ?>" />-->
        <!--<link href="../../web/css/bootstrap.min.css" rel="stylesheet" media="screen">-->
         <link href="http://getbootstrap.com/dist/css/bootstrap.css" rel="stylesheet" />
         <link rel="stylesheet" type="text/css" href="<?php echo '../web/css/'.Config::$css ?>" />
       <script type="text/javascript">
           window.onload = function(){
            este=document.getElementById("tablaPresupuestosCliente");
	   document.getElementById("presupuestoscliente").onclick = esconder;
          
          function esconder(){
             
              if (este.getAttribute('display') === 'block'){ 
                 document.getElementById("tablaPresupuestosCliente").setAttribute('display','none');
               }else{
                 document.getElementById("tablaPresupuestosCliente").setAttribute('display', 'block') ; 
               }
        }
    };

    </script>
    </head>
    <body>
        <?php foreach ($params[0] as $cliente) : ?>
            <?php $id = $cliente->getId(); ?> 
         <?php endforeach ?>
        <div id="menu">
            <hr/>
                <a href="index.php?ctl=añadirArticulosCliente&id=<?php echo $id ; ?>">Agregar Articulo al cliente</a> ||
                <a href="index.php?ctl=añadirPresupuestoCliente">Agregar Presupuesto al cliente</a> |
                <a href=""></a> |
                <a href="index.php?ctl=añadirVisitaCliente">Añadir Visitas a Cliente</a> |                  <!--index.php?ctl=buscar-->
                <a href=""></a> |                 <!--index.php?ctl=buscarAlimentosPorEnergia-->
                <a href="index.php?ctl=menuClientes">Menú Principal Clientes</a>       <!---->
            <hr/>
        </div>
        <h3 id="CabeceraListado"><a href="#articuloscliente"> Artículos </a><a href="#presupuestoscliente"> Presupuestos </a><a href="#visitascliente"> Visitas </a> </h3>
        <div>
             <table id="datosclientes" border='1' width='1000'>
                 <td>Código Cliente</td><td>Razón Social</td><td>Dirección</td><td>Población</td>
                 <td>Provincia</td><td>Codigo Postal</td><td>Teléfono</td><td>Contacto</td>
                 <br>
                  <?php foreach ($params[0] as $cliente) : ?>
                  <?php $idcli=$cliente->getId(); ?>
                  <?php $_SESSION['clientearticulo'] =  $idcli; ?>
                 <tr>
                    <td><?php echo $cliente->getId() ?> </td>  
                    <td><?php echo $cliente->getRazonSocial() ?> </td>
                    <td><?php echo $cliente->getDireccion() ?> </td>
                    <td><?php echo $cliente->getPoblacion() ?> </td>
                    <td><?php echo $cliente->getProvincia() ?> </td>
                    <td><?php echo $cliente->getCodigoPostal() ?> </td>
                    <td><?php echo $cliente->getTelefono() ?> </td>
                    <td><?php echo $cliente->getContacto() ?> </td>
                  <br>
                 </tr>
             <?php endforeach ?>
                 
          
             </table>
            <br>
        </div>
        <div class=”container">
            <div id="articuloscliente"> Articulos Cliente <br>
            <!-- Articulos del cliente -->
            <table id="articuloscliente" border='1' width='700'>
                <?php $basedatos=""; ?>
                <th>
                Cod.SAP<td></td><td>Tipo Caja</td><td>Descripción</td><td>largo</td><td>ancho</td><td>alto</td><td>calidad</td><td>precio x 1000</td>
                </th>
                <?php $idcli=$_SESSION['clientearticulo'];?>
                <?php foreach ($params[3] as $articulo) : ?> 
                <tr id="articulos">
                    
                    <td><a href="index.php?ctl=verUnArticulo&id=<?php echo $articulo->getcodigo(); ?>"><?php echo $articulo->getcodigo(); ?></a></td>
                    <td><?php echo $articulo->getcodigocliente(); ?></td>
                    <td><?php echo sacaCampo($basedatos, "id", $articulo->gettipocaja(), "Descripcion" , "tiposcaja"); ?></td>
                    <td><?php echo $articulo->getdescripcion(); ?></td>
                    <td><?php echo $articulo->getlargo(); ?></td>
                    <td><?php echo $articulo->getancho(); ?></td>
                    <td><?php echo $articulo->getalto(); ?></td>
                    <td><?php echo sacaCampo($basedatos, "calidad", $articulo->getcalidad(), "Descripcion", "calidades"); ?></td>
                    <td><?php echo $articulo->getprecio(); ?></td>
                 </tr>
                
                 <?php endforeach ?>
                
            </table>
        
            </div><br>
            <div class="row">
                
            <div id="presupuestoscliente" class="col-sm-8"> 
                <span>Presupuestos Cliente <a id="arriba" href="#menu"> arriba </a></span><br>
            <table border='1' width='500' id='tablaPresupuestosCliente'> 
         <!-- Presupuestos Cabecera-->
          
            <?php foreach ($params[1] as $cliente) : ?> 
              <tr id="cabeceraTablaInformacionCliente">
                       
                  <td><a href="index.php?ctl=verUnPresupuesto&id=<?php echo $cliente->getId(); ?>"><?php echo $cliente->getId(); ?></a></td>
              <td> <?php echo ('Fecha:') . trim($cliente->getfecha()); ?>   <!--</td>
              <td>--> <?php echo ('   Descripción :  '); ?></td>
              <td> <?php echo ('Precio  ') ; ?></td>
              
              </tr>
              <?php $estaCabecera = $cliente->getId(); ?>
              <!-- Presupuesto Lineaa -->
              
                <?php foreach ($params[2] as $lineas) : ?> 
                    <?php foreach ($lineas as $lineacliente) : ?>
                      <?php  if ($estaCabecera == $lineacliente->getIdCabecera()){ ?>
              <tr><td><?php echo $lineacliente->getIdCabecera(); ?></td>
                            <td> <?php echo trim($lineacliente->getlinea()); ?>
                              <?php echo " | " . $lineacliente->getdescripcion(); ?></td>
                            <td> <?php echo $lineacliente->getprecio(); ?></td></tr>
                        <?php } ?>
                    <?php endforeach ?>
                <?php endforeach ?>
            </tr>
            <?php endforeach ?>
           </tr>
            </table>
            </div>
                <br>
            <div id="visitascliente" class="col-sm-4">
                Visitas Cliente<a id="arriba" href="#menu">arriba </a><br>
                <table border='1' width='450' id='tablaVisitasCliente'> 
                    <?php foreach ($params[4] as $lineas) : ?> 
                      <?php foreach ($lineas as $lineavisita) : ?>
                    <?php $date=date(' j \ F \ Y', strtotime($lineavisita->getFecha())); ?>
                    <!--<td><php echo $lineavisita->getId();?></td>-->
                        <tr><td><?php echo $date ;?></td>
                        <td><?php echo $lineavisita->getTexto();?></td>
                        <td><?php echo $lineavisita->getEstado();?></td></tr>
                     <?php endforeach ?>
                     <?php endforeach ?>
                </table>
                
            </div>
           
            </div>
      </div>
         
    </body>
         
    <div id='a_pie'>
    <?php pie() ?>
    </div>
    
</html>