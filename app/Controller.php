<?php
include_once '/modelo/ModeloInterface.php';
include_once 'funciones.php';
error_reporting(E_ALL);
class Controller {
 function login(){
    if (isset($_POST['usuario'])) {
                if (($_POST['usuario'] == 'admin') && ($_POST['pwd'] == 'admin')) {
                    $_SESSION["nombre"]="admin";
                    $_SESSION["activa"]=true;
                    header('Location: index.php?ctl=menuMain');
                }else{
                    header('Location: index.php?ctl=login');
                }
    }else {
        $params = array(
           'usuario' => 'admin',
           'pwd' => 'admin');
        require __DIR__ . '/templates/login.php';
                
    }
}

function menuClientes(){
    $_SESSION['contadorgrid']=0;
    $_SESSION['contadorlineas']=0;
    $modelo=obtenerModelo();
    $clientes= $modelo->getClientes();
    $params=array(
            'clientes'=>$clientes,
            'mensaje' =>"Listado de Clientes"
        
        );
    require __DIR__.'/templates/listarClientes.php';
}

function añadirArticulosCliente(){
     $modelo= obtenerModelo();
     $id=$_GET['id'];
     $cliente=$modelo->getClienteId($id);
     $tiposcaja=$modelo->getTiposCaja();
     $calidades=$modelo->getCalidades();
     $params=array('cliente' =>$cliente,
                   'tipocaja' =>$tiposcaja,
                   'calidades' =>$calidades);
     require __DIR__ .'/templates/anadirArticuloCliente.php';
    
}

function grabarArticulo(){
    $modelo = obtenerModelo();
    $articulo = new Articulos("","","","","","","","","");
    if ($_SERVER){
        $codigo= ($_POST['codigo']);
        $codigo= recoge($codigo);
        $codigocliente= ($_POST['codigocliente']);
        $codigocliente= recoge($codigocliente);
        $id=$codigocliente;
        $tipocaja= ($_POST['tiposcaja']);
        $tipocaja= recoge($tipocaja);
        $descripcion= ($_POST['descripcion']);
        $descripcion= recoge($descripcion);
        $largo= ($_POST['largo']);
        $largo= recoge($largo);
        $ancho= ($_POST['ancho']);
        $ancho= recoge($ancho);
        $alto= ($_POST['alto']);
        $alto= recoge($alto);
        $calidad= ($_POST['calidad']);
        $calidad= recoge($calidad);
        $precio= ($_POST['precio']);
        $precio= recoge($precio);
        $articulo->setcodigo($codigo);
        $articulo->setcodigocliente($codigocliente);
        $articulo->settipocaja($tipocaja);
        $articulo->setdescripcion($descripcion);
        $articulo->setlargo($largo);
        $articulo->setancho($ancho);
        $articulo->setalto($alto);
        $articulo->setcalidad($calidad);
        $articulo->setprecio($precio);
        $modelo->grabarArticulo($articulo);
        $this->recargaCliente($id);
     }
}

function añadirPresupuestoCliente(){
    include_once 'DataGrid.php';
    $modelo= obtenerModelo();
    $id=$_SESSION['clientearticulo'];
    $cliente=$modelo->getClienteId($id);
    $tiposcaja=$modelo->getTiposCaja();
    $calidades=$modelo->getCalidades();
    $presupCab=$modelo->getCabPrespuesto(0);
    $presupLin=$modelo->getDetallePresupuesto(0);
    $modelo->inicializaTemporal();
    $params=array('cliente' =>$cliente,
                   'tipocaja' =>$tiposcaja,
                   'calidades' =>$calidades,
                    'presupCab'=>$presupCab,
                    'presupLin'=>$presupLin);
                    
     require __DIR__ .'/templates/anadirPresupuestosCliente2.html';
    
}

function grabarPresupuesto(){
    $hoy=date("d/") . date("m/") . date("y");
    $modelo = obtenerModelo();
    $presupuestoCab = new PresupuestoCabecera("","","");
    
    $siguiente= sacaultimo($modelo, "id",  "presupuestocabecera");
    $presupuestoCab->setId($siguiente);
    $presupuestoCab->setFecha($hoy);
    $presupuestoCab->setIdCli($_POST['codClte']);
    $lineasdetalle=count($_POST['cod1']);
    $siguiente= sacaultimo($modelo, "id",  "presupuestolinea");
    for ($i=0;$i<$lineasdetalle;$i++){
       $presupuestoLin[$i] = new PresupuestoDetalle("","","","","","","");
       $codigo=$_POST['cod1'];
       $descripcion=$_POST['desc3'];
       $escalado=$_POST['esc4'];
       $precio=$_POST['prec5'];
       $presupuestoLin[$i]->setId($siguiente);
       $presupuestoLin[$i]->setIdCabecera($presupuestoCab->getId());
       $presupuestoLin[$i]->setLinea($i+1);
       $presupuestoLin[$i]->setCodigo($codigo[$i]);
       $presupuestoLin[$i]->setDescripcion($descripcion[$i]);
       $presupuestoLin[$i]->setEscalado($escalado[$i]);
       $presupuestoLin[$i]->setPrecio($precio[$i]);
       $siguiente++;
    }
    
    $modelo->setPresupuestos($presupuestoCab, $presupuestoLin);
    //$cliente=$modelo->getClienteId($presupuestoCab->getIdCli());
    call_user_func($this->recargaCliente());
    
}

function verUnPresupuesto(){
     $modelo= obtenerModelo();
     $idPresupuesto=$_GET['id'];
     $presupCab=$modelo->getCabeceraPresupuesto($idPresupuesto);
     $presupLin=$modelo->getDetallePresupuesto($idPresupuesto);
     $idCli=$presupCab[0];
     $cliente=$modelo->getClienteId($idCli->getIdCli());
     $url='http://localhost/Prespuestos/web/index.php?ctl=verUnPresupuesto&id=<?php echo $idPresupuesto; ?>';
     $params=array('cliente'=>$cliente,
                    'cabecera'=>$presupCab,
                    'lineas'=>$presupLin,
                    'url' =>$url);
     require __DIR__ .'/templates/verUnPresupuesto.php';
    
}
function añadirVisitaCliente(){
    $modelo= obtenerModelo();
    $id=$_SESSION['clientearticulo'];
    $cliente=$modelo->getClienteId($id);
   
    $params=array('cliente' =>$cliente,
                  );
                    
     require __DIR__ .'/templates/anadirVisitaCliente.html';
    
    
}

function grabarVisita(){
    
    
}
function menuPresupuestos(){
    
    
}
function menuMain(){
    $params= [
        'mensaje' =>'Menu Principal',
        'usuario' =>$_SESSION['nombre'],
        ];
    $contenido="";
    $_SESSION['clientearticulo']="";
    require __DIR__ .'\templates\menuMain.php';
}
public function cerrarSesion() {
        session_start();
        $_SESSION['activa'] = 0;
        $_SESSION['usuario']="";
        session_destroy();
        header("Location: index.php");
    }
    
 public function verPresupuestosCliente(){
     $modelo= obtenerModelo();
     $id=$_GET['id'];
     $cliente=$modelo->getClienteId($id);
     $presupuestosCab=$modelo->getCabPresupuestos_Cliente($id);
     $params=$cliente;
     // Hay que añadir arriba    'presupuestos' => $presupuestosCab
     require __DIR__ .'/templates/presupuestosCliente.php';
 }
 
 public function menuUnCliente(){
     $modelo= obtenerModelo();
     $id=$_GET['id'];
     $cliente=$modelo->getClienteId($id);
     $presupuestosCab=$modelo->getCabPresupuestos_Cliente($id);
     $visitas[]=$modelo->getVisitasCliente($id);
     $contador=0;
     $idCab="";
     foreach ($presupuestosCab as $cabPresup){
      $presupuestosLin[$contador]= new PresupuestoDetalle("","","","","");
      $idCab = $cabPresup->getId();
     $presupuestosLin[$contador]= $modelo->getDetallePresupuesto($idCab);
     $contador++;   
     }
     $articuloscliente=$modelo->getArticulosCliente($id);
     
     // recogería las visitas
     // recogería articulos ..........
     
     $params[0]=$cliente;
     $params[1]=$presupuestosCab;
     $params[2]=$presupuestosLin;
     $params[3]=$articuloscliente;
     $params[4]=$visitas;
     require __DIR__ .'/templates/informacionCliente.php';
 }
 public function recargaCliente(){
     $modelo= obtenerModelo();
     $id=$_SESSION['clientearticulo'];
     $cliente=$modelo->getClienteId($id);
     $presupuestosCab=$modelo->getCabPresupuestos_Cliente($id);
     $contador=0;
     $idCab="";
     foreach ($presupuestosCab as $cabPresup){
      $presupuestosLin[$contador]= new PresupuestoDetalle("","","","","");
      $idCab = $cabPresup->getId();
     $presupuestosLin[$contador]= $modelo->getDetallePresupuesto($idCab);
     $contador++;   
     }
     $articuloscliente=$modelo->getArticulosCliente($id);
     
     // recogería las visitas
     $visitas[]=$modelo->getVisitasCliente($id);
     // recogería articulos ..........
     
     $params[0]=$cliente;
     $params[1]=$presupuestosCab;
     $params[2]=$presupuestosLin;
     $params[3]=$articuloscliente;
     $params[4]=$visitas;
     require __DIR__ .'/templates/informacionCliente.php';
     
 }
 
 
 public function verUnArticulo(){
     $modelo= obtenerModelo();
     $id=$_GET['id'];
    //$codArt=$codigoArticulo;
     $articulo=$modelo->getArticulo($id);
    $params[0]=$articulo;
     $imagen=$modelo->getimagenArticulo($id);
     $params[1]=$imagen;
     //"index.php?ctl=verUnArticulo&id=<?php echo $articulo->getcodigo(); >">
     $url="http://localhost/Prespuestos/web/index.php?ctl=verUnArticulo&id=<?php echo $id; ?>";
     $params[2]=$url;
     
     require  __DIR__ .'/templates/verUnArticulo.php';
     
     
 }
 
 

}