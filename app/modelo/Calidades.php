<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Calidades
 *
 * @author Jose Montes <your.name at your.org>
 */
class Calidades {
    private $calidad;
    private $descripcion;
    private $abreviatura;
    
    function __construct($calidad,$descripcion,$abreviatura) {
        $this->calidad=$calidad;
        $this->descripcion=$descripcion;
        $this->abreviatura=$abreviatura;
    }
    
    function getCalidad(){
        return $this->calidad;
    }
    function setCalidad($id){
        $this->calidad=$id;
    }
    function getDescripcion(){
        return $this->descripcion;
    }
    function setDescripcion($descripcion){
        $this->descripcion=$descripcion;
    }
    function getAbreviatura(){
        return $this->abreviatura;
    }
    function setAbreviatura($abreviatura){
        $this->abreviatura=$abreviatura;
    }
}
