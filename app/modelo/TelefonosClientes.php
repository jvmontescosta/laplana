<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TelefonosClientes
 *
 * @author Jose Montes <your.name at your.org>
 */
class TelefonosClientes {
    //put your code here
    private $id;
    private $codigoCliente;
    private $personaContacto;
    private $cargo;
    private $telefono;
    private $email;
    
    public function getId() {
        return $this->id;        
    }
    public function setId($id) {
        $this->id=$id;
    }
    public function getcodigoCliente() {
        return $this->codigoCliente;        
    }
    public function setcodigoCliente($codigoCliente) {
        $this->codigoCliente=$codigoCliente;
    }
    public function getpersonaContacto() {
        return $this->personaContacto;        
    }
    public function setpersonaContacto($personaContacto) {
        $this->personaContacto=$personaContacto;
    }
    public function getcargo() {
        return $this->cargo;        
    }
    public function setcargo($cargo) {
        $this->cargo=$cargo;
    }
    public function gettelefono() {
        return $this->telefono;        
    }
    public function settelefono($telefono) {
        $this->telefono=$telefono;
    }
    public function getemail() {
        return $this->email;        
    }
    public function setemail($email) {
        $this->email=$email;
    }
}
