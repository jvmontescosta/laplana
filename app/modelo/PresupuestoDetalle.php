<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PresupuestoDetalle
 *
 * @author Jose Montes <your.name at your.org>
 */
class PresupuestoDetalle {
    private $id;
    private $idCabecera;
    private $linea;
    private $codigo;
    private $descripcion;
    private $escalado;
    private $precio;
    
    public function __construc($id,$idCabecera,$linea,$codigo,$descripcion,$escalado,$precio){
        $this->$id=$id;
        $this->$idCabecera=$idCabecera;
        $this->$linea=$linea;
        $this->$codigo=$codigo;
        $this->$descripcion=$descripcion;
        $this->$escalado=$escalado;
        $this->$precio=$precio;
        
    }
   
     public function getId() {
        return $this->id;
    }
    public function setId($id){
        $this->id=$id;
    }
     public function getIdCabecera() {
        return $this->idCabecera;
    }
    public function setIdCabecera($idCab){
        $this->idCabecera=$idCab;
    }
     public function getLinea() {
        return $this->linea;
    }
    public function setLinea($linea){
        $this->linea=$linea;
    }
    
     public function getCodigo() {
        return $this->codigo;
    }
    public function setCodigo($codigo){
        $this->codigo=$codigo;
    }
     public function getDescripcion() {
        return $this->descripcion;
    }
    public function setDescripcion($descripcion){
        $this->descripcion=$descripcion;
    }
    public function getEscalado() {
        return $this->escalado;
    }
    public function setEscalado($escalado){
        $this->escalado=$escalado;
    }
    
     public function getPrecio() {
        return $this->precio;
    }
    public function setPrecio($precio){
        $this->precio=$precio;
    } 
    
}
