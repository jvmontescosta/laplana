<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ObservacionesClientes
 *
 * @author Jose Montes <your.name at your.org>
 */
class ObservacionesClientes {
    private $codigo;
    private $codigoCliente;
    private $fecha;
    private $observaciones;
    
    public function getcodigo() {
        return $this->codigo;
    }
    public function setcodigo($codigo){
        $this->codigo=$codigo;
    }
    public function getcodigoCliente() {
        return $this->codigoCliente;
    }
    public function setcodigoCliente($codigoCliente){
        $this->codigoCliente=$codigoCliente;
    }
    public function getFecha() {
        return $this->Fecha;
    }
    public function setFecha($Fecha){
        $this->Fecha=$Fecha;
    }
    public function getobservaciones() {
        return $this->observaciones;
    }
    public function setobservaciones($observaciones){
        $this->observaciones=$observaciones;
    }
    //put your code here
}
