<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TiposCaja
 *
 * @author Jose Montes <your.name at your.org>
 */
class TiposCaja {
    private $id;
    private $descripcion;
    private $abreviatura;
    
    function __construct($id,$descripcion,$abreviatura) {
        $this->id=$id;
        $this->descripcion=$descripcion;
        $this->abreviatura=$abreviatura;
    }
    
    function getId(){
        return $this->id;
    }
    function setId($id){
        $this->id=$id;
    }
    function getDescripcion(){
        return $this->descripcion;
    }
    function setDescripcion($descripcion){
        $this->descripcion=$descripcion;
    }
    function getAbreviatura(){
        return $this->abreviatura;
    }
    function setAbreviatura($abreviatura){
        $this->abreviatura=$abreviatura;
    }
}
