<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Clientes
 *
 * @author Jose Montes <your.name at your.org>
 */
class Clientes {
    private $id;
    private $razonsocial;
    private $direccion;
    private $poblacion;
    private $provincia;
    private $codigopostal;
    private $telefono;
    private $contacto;
    
    public function __construct($id , $razonsocial, $direccion, $poblacion,$provincia,$codigopostal,$telefono,$contacto){
    $this->id = $id;
    $this->razonsocial = $razonsocial;
    $this->direccion = $direccion;
    $this->poblacion = $poblacion;
    $this->provincia = $provincia;
    $this->codigopostal = $codigopostal;
    $this->telefono = $telefono;
    $this->contacto = $contacto;
    }

    public function getId() {
        return $this->id;
    }
    public function setId($id){
        $this->id=$id;
    }
    public function getRazonSocial() {
        return $this->razonsocial;
    }
    public function setRazonSocial($razon){
        $this->razonsocial=$razon;
    }
    public function getDireccion() {
        return $this->direccion;
    }
    public function setDireccion($direccion){
        $this->direccion=$direccion;
    }
    public function getPoblacion() {
        return $this->poblacion;
    }
    public function setPoblacion($poblacion){
        $this->poblacion=$poblacion;
    }
    public function getProvincia() {
        return $this->poblacion;
    }
    public function setProvincia($provincia){
        $this->provincia=$provincia;
    }
    public function getCodigoPostal() {
        return $this->codigopostal;
    }
    public function setCodigoPostal($cp){
        $this->codigopostal=$cp;
    }
    public function getTelefono() {
        return $this->telefono;
    }
    public function setTelefono($telefono){
        $this->telefono=$telefono;
    }
     public function getContacto() {
        return $this->contacto;
    }
    public function setContacto($contacto){
        $this->contacto=$contacto;
    }
}

