<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HorariosDescargaClientes
 *
 * @author Jose Montes <your.name at your.org>
 */
class HorariosDescargaClientes {
    //put your code here
    private $id;
    private $diaSemana;
    private $horaInicio;
    private $horaFin;
    private $codigoCliente;
    
    public function getId() {
        return $this->id;
    }
    public function setId($id){
        $this->id=$id;
    }
    public function getdiaSemana() {
        return $this->diaSemana;
    }
    public function setdiaSemana($diaSemana){
        $this->diaSemana=$diaSemana;
    }
    public function gethoraInicio() {
        return $this->horaInicio;
    }
    public function sethoraInicio($horaInicio){
        $this->horaInicio=$horaInicio;
    }
    public function gethoraFin() {
        return $this->horaFin;
    }
    public function sethoraFin($horaFin){
        $this->horaFin=$horaFin;
    }
    public function getcodigoCliente() {
        return $this->codigoCliente;
    }
    public function setcodigoCliente($codigoCliente){
        $this->codigoCliente=$codigoCliente;
    }
}
