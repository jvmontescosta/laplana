<?php
require_once 'Clientes.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PresupuestoCabecera
 *
 * @author Jose Montes <your.name at your.org>
 */
class PresupuestoCabecera {
  
  
  private $id;
  private $fecha;
  private $idCli;
    
 public function __construct($id, $fecha, $idCli) {
        $this->id = $id;
        $this->fecha = $fecha;
        $this->idCli = $idCli;
     }

    public function getId() {
        return $this->id;
    }
    public function setId($id){
        $this->id=$id;
    }
    public function getFecha() {
        return $this->fecha;
    }
    public function setFecha($fecha){
        $this->fecha=$fecha;
    }
    public function getIdCli(){
        return $this->idCli;
    }
    public function setIdCli($idCli){
       $this->idCli=$idCli;
    }
}
