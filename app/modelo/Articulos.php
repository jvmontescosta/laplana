<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Articulos
 *
 * @author Jose Montes <your.name at your.org>
 */
class Articulos {
   private $codigo;
   private $codigocliente;
   private $tipocaja;
   private $descripcion;
   private $largo;
   private $ancho;
   private $alto;
   private $calidad;
   private $precio;
   
   public function __construct($codigo,$codigocliente,$tipocaja,$descripcion,$largo,$ancho,$alto,$calidad,$precio) {
      $this->codigo=$codigo;
      $this->codigocliente=$codigocliente;
      $this->tipocaja =$tipocaja;
      $this->descripcion=$descripcion;
      $this->largo=$largo;
      $this->ancho=$ancho;
      $this->alto=$alto;
      $this->calidad=$calidad;
      $this->precio=$precio;
     }
    public function getcodigo(){
        return $this->codigo;
    }
    public function setcodigo($codigo){
        $this->codigo=$codigo;
    }
    public function getcodigocliente(){
        return $this->codigocliente;
    }
    public function setcodigocliente($codigocliente){
        $this->codigocliente=$codigocliente;
    }
    public function gettipocaja(){
        return $this->tipocaja;
    }
    public function settipocaja($tipocaja) {
        $this->tipocaja=$tipocaja;
    }
    public function getdescripcion() {
        return $this->descripcion;
    }
    public function setdescripcion($descripcion) {
        $this->descripcion=$descripcion;
    }
    public function getlargo(){
        return $this->largo;
    }
    public function setlargo($largo) {
        $this->largo=$largo;
    }
    public function  getancho(){
        return $this->ancho;
    }
    public function setancho($ancho){
        $this->ancho=$ancho;
    }
    public function getalto() {
        return $this->alto;
    }
    public function setalto($alto) {
        $this->alto=$alto;
    }
    public function getcalidad(){
        return $this->calidad;
    }
    public function setcalidad($calidad){
        $this->calidad=$calidad;
    }
    public function getprecio(){
        return $this->precio;
    }
    public function setprecio($precio){
        $this->precio=$precio;
    }
        
   
}
