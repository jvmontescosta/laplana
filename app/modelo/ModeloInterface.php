<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jose Montes <your.name at your.org>
 */
interface ModeloInterface {
    
    public function getClientes();
    
  // public function getRepresentante($idRepre);
    
    public function grabarCliente($miCliente);
    
  //  public function grabarRepresentante($miRepresentante);

    public function instalar();
    
  //  public function getRepresentantes();
    
    public function ultimoCliente();
    
  //  public function ultimoRepresetante();
    
    public function validar($dato);
    
  //  public function datosRepresValidos($nom,$apell);
    
    public function datosClientValidos($nom,$apell,$idRep);
    
    public function getClienteId($id);
    
    public function getCabPrespuesto($id);
    
    public function getCabPresupuestos_Cliente($idCliente);
    
    public function getDetallePresupuesto($id);
    


}

