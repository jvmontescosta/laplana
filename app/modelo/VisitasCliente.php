<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VisitasCliente
 *
 * @author Jose Montes <your.name at your.org>
 */
class VisitasCliente {
    private $id;
    private $fecha;
    private $codigoCliente;
    private $texto;
    private $estado;
    
  public function __construct($id,$fecha,$codigoCliente,$texto,$estado) {
     $this->id=$id;
     $this->fecha=$fecha;
     $this->codigoCliente=$codigoCliente;
     $this->texto=$texto;
     $this->estado=$estado;
      
  }
  public function getId(){
      return $this->id;
  }
  public function  setId($id){
      $this->id=$id;
  }
  
  public function getFecha(){
      return $this->fecha;
  }
  public function  setFecha($fecha){
      $this->fecha=$fecha;
  }
 public function getCodigoCliente(){
      return $this->codigoCliente;
  }
  public function  setCodigoCliente($codigoCliente){
      $this->codigoCliente=$codigoCliente;
  }
  
  public function getTexto(){
      return $this->texto;
  }
  public function  setTexto($texto){
      $this->texto=$texto;
  }
     public function getEstado(){
      return $this->estado;
  }
  public function  setEstado($estado){
      $this->estado=$estado;
  }
}
