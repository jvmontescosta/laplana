<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModeloMySql
 *
 * @author Jose Montes <your.name at your.org>
 */

error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once 'modBasesDatos.php';
include_once 'PresupuestoCabecera.php';
include_once 'PresupuestoDetalle.php';
include_once 'Clientes.php';
include_once 'Articulos.php';
include_once 'TiposCaja.php';
include_once 'Calidades.php';
include_once 'VisitasCliente.php';

 class ModeloMySql  implements ModeloInterface  {
    //put your code here
    protected $miconexion;
     
     public function __construct($dbname, $dbuser, $dbpass, $dbhost){
        //  $mdb = conectaDB($bd,$us,$pw); 
          $this->miconexion =  NULL;
        try {
            //   $bdconexion = new PDO('mysql:host=' . $dbhost . ';dbname='
            //  . $dbname . ';charset=utf8', $dbuser, $dbpass);
                $bdconexion = new PDO('mysql:host=' . $dbhost . ';dbname='
              . $dbname . ';charset=utf8', $dbuser, $dbpass);
            $this->miconexion = $bdconexion;
            $this->miconexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
         
     }
    public function getClientes() {
          //  $bd=$us=$pw="";
          //  $bd=Config::$bdnombre;
          //  $us=Config::$bdusuario;
          //  $pw=Config::$bdclave;
    try  {
              //  $mdb = conectaDB($bd,$us,$pw); 
            $CsqlRespuestas = "select * from clientes";
            $resultado=$this->miconexion->query($CsqlRespuestas);
            $clientes[] = new Clientes("", "", "", "", "", "", "", "");
            $count=0;
             while ($linea = $resultado->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                     // $datos = $fila[0] . "\t" . $fila[1] . "\t" . $fila[2] . "\n";
                     $id=$razonsocial=$direccion=$poblacion=$provincia=$codigopostal=$contacto=$contacto="";
                     $id=$linea[0];
                     $razonsocial=$linea[1];
                     $direccion=$linea[2];
                     $poblacion=$linea[3];
                     $provincia=$linea[4];
                     $codigopostal=$linea[5];
                     $telefono=$linea[6];
                     $contacto=$linea[7];
                     $clientes[$count] = new Clientes("", "", "","","","","","");
                     $clientes[$count]->setId($id);
                     $clientes[$count]->setRazonSocial($razonsocial);
                     $clientes[$count]->setDireccion($direccion);
                     $clientes[$count]->setPoblacion($poblacion);
                     $clientes[$count]->setProvincia($provincia);
                     $clientes[$count]->setCodigoPostal($codigopostal);
                     $clientes[$count]->setTelefono($telefono);
                     $clientes[$count]->setContacto($contacto);
                    // echo ("clientes[count] en ModeloMySql! ".$clientes[$count]."<br>");
                     $count++;
      
             }
          $this->miconexion= false;
          return $clientes;
      
    } catch (Exception $e ) {
        echo $e->getMessage();
    }
    
    }
   public function grabarCliente($miCliente) {
       /*     $bd=$us=$pw="";
            $bd=Config::$bdnombre;
            $us=Config::$bdusuario;
            $pw=Config::$bdclave; 
         */   
        
        try{
           // $mdb = conectaDB($bd,$us,$pw);
        
        $id=$razonsocial=$direccion=$poblacion=$provincia=$codigopostal=$telefono=$contacto="";
        $id=$miCliente->getId();
        $razonsocial=$miCliente->getRazonSocial();
        $direccion=$miCliente->getDireccion();
        $poblacion=$miCliente->getPoblacion();
        $provincia=$miCliente->getProvincia();
        $codigopostal=$miCliente->getCodigoPostal();
        $telefono=$miCliente->getTelefono();
        $$contacto=$miCliente->getContacto();
         
        $Csql="Insert into clientes(id,razonsocial,direccion,poblacion,provincia,codigopostal,telefono,contacto) VALUES (?,?,?,?,?,?,?,?)";
       // $result=$mdb->query($Csql);
        $Csql=$this->miconexion->prepare("Insert into clientes(id,razonsocial,direccion,poblacion,provincia,codigopostal,telefono,contacto) 
                                        VALUES (?,?,?,?,?,?,?,?)");
                
        $Csql->bindParam(1, $id);
        $Csql->bindParam(2, $razonsocial);
        $Csql->bindParam(3, $direccion);
        $Csql->bindParam(4, $poblacion);
        $Csql->bindParam(5, $provincia);
        $Csql->bindParam(6, $codigopostal);
        $Csql->bindParam(7, $telefono);
        $Csql->bindParam(8, $contacto);
        $Csql->execute();
        $clienteResult = New Clientes($id, $razonsocial, $direccion,$poblacion,$provincia,$codigopostal,$telefono,$contacto);
        //echo "Grabado";
        return $clienteResult;
        } catch (Exception $e) {
            print ("Error al ir a grabar cliente ". $e->getMessage()  );
        }
        
        
    }
     public function grabarArticulo($Articulo){
        $codigo=$codigocliente=$tipocaja=$descripcion=$largo=$ancho=$alto=$calidad="";
        $precio="0";
        try{
        $codigo=$Articulo->getCodigo(); 
        $codigocliente=$Articulo->getCodigoCliente();
        $tipocaja=$Articulo->getTipoCaja();
        $descripcion=$Articulo->getDescripcion();
        $largo=$Articulo->getlargo();
        $ancho=$Articulo->getancho();
        $alto=$Articulo->getalto();
        $calidad=$Articulo->getcalidad();
        $precio=$Articulo->getprecio();
      //   $Csql="Insert into articulos(codigo,codigocliente,tipocaja,descripcion,largo,ancho,alto,calidad,precio) VALUES (?,?,?,?,?,?,?,?,?)";
       // $result=$mdb->query($Csql);
        $Csql=$this->miconexion->prepare("INSERT INTO articulos(Codigo,CodigoCliente,TipoCaja,Descripcion,largo,ancho,alto,Calidad,Precio) 
                                        VALUES (:codigo,:codigocliente,:tipocaja,:descripcion,:largo,:ancho,:alto,:calidad,:precio)");
            //    $stmt = $conn->prepare("INSERT INTO MyGuests (firstname, lastname, email)
                 // VALUES (:firstname, :lastname, :email)");
        $Csql->bindParam(':codigo', $codigo);
        $Csql->bindParam(':codigocliente', $codigocliente);
        $Csql->bindParam(':tipocaja', $tipocaja);
        $Csql->bindParam(':descripcion', $descripcion);
        $Csql->bindParam(':largo', $largo);
        $Csql->bindParam(':ancho', $ancho);
        $Csql->bindParam(':alto', $alto);
        $Csql->bindParam(':calidad', $calidad);
        $Csql->bindParam(':precio', $precio);
        $result=$Csql->execute();
         } catch (Exception $e) {
            print ("Error al ir a grabar el articulo ". $e->getMessage()  );
        }  
 
    }

   public function instalar() {
        echo "<h2>Instalando: " . Config::$modelo . "</h2>";
        try {
            // Conectamos sin indicar bbdd
            $conexion = new PDO("mysql:host=" . Config::$bdhostname . ".", Config::$bdusuario, Config::$bdclave);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }


        //creamos la base de datos si no existe
        try {
            $crear_bd = $conexion->prepare('CREATE DATABASE IF NOT EXISTS ' . Config::$bdnombre . ' COLLATE utf8_spanish_ci');
            $crear_bd->execute();
            echo "Creada BDDD: " . Config::$bdnombre . "<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }


        //decimos que queremos usar la BBDD que acabamos de crear
        try {
            $use_db = $conexion->prepare('USE ' . Config::$bdnombre);
            $use_db->execute();
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }


        try {
            
         $sql ='CREATE TABLE clientes(
                id int(6) NOT NULL,
                razonsocial varchar(30) COLLATE utf8_spanish_ci NOT NULL,
                direccion varchar(30) COLLATE utf8_spanish_ci NOT NULL,
                poblacion varchar(30) COLLATE utf8_spanish_ci NOT NULL,
                provincia varchar(30) COLLATE utf8_spanish_ci NOT NULL,
                codigopostal varchar(6) COLLATE utf8_spanish_ci NOT NULL,
                telefono varchar(10) COLLATE utf8_spanish_ci NOT NULL,
                contacto varchar(20) COLLATE utf8_spanish_ci NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;';

            $crear_tb_clientes = $this->miconexion->prepare($sql);
            $crear_tb_clientes->execute();
            
            echo "Creada tabla: Clientes <br>";
    // ==================================================================================================        
            $sql ='CREATE TABLE presupuestocabecera (
                    id int(6) NOT NULL,
                    codigocliente int(6) NOT NULL,
                    fecha date NOT NULL
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;';
            
            $crear_tb_presupuestocabecera = $this->miconexion->prepare($sql);
            $crear_tb_presupuestocabecera->execute();
             echo "Creada tabla: Presupuesto Cabecera<br>";
     
     // ===========================================================
             
           $sql = 'CREATE TABLE IF NOT EXISTS presupuestolinea (
            id int(6) NOT NULL,
            idCabecera int(6) NOT NULL,
            linea int(2) NOT NULL,
            descripcion varchar(40) COLLATE utf8_spanish_ci NOT NULL,
            precio double NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;';
            $crear_tb_presupuestolinea = $this->miconexion->prepare($sql);
            $crear_tb_presupuestolinea->execute();
             echo "Creada tabla: Presupuesto Linea<br>";
             
      // ===============================================================================
             
           $sql='ALTER TABLE clientes
            ADD PRIMARY KEY (id);';
           $crear_tb_crearindices = $this->miconexion->prepare($sql);
           $crear_tb_crearindices->execute();
           echo "Creando indices <br>";
           
/*
--
-- Indices de la tabla `presupuestocabecera`
--
 * */
         

        $sql='ALTER TABLE presupuestocabecera
            ADD PRIMARY KEY (id),
            ADD KEY codigocliente (codigocliente);';
            $crear_tb_crearindices = $this->miconexion->prepare($sql);
            $crear_tb_crearindices->execute();
/*
-- Indices de la tabla `presupuestolinea`
*/
           
        $sql='ALTER TABLE presupuestolinea
                ADD PRIMARY KEY (id),
                ADD KEY idCabecera (idCabecera);';
            $crear_tb_crearindices = $this->miconexion->prepare($sql);
            $crear_tb_crearindices->execute();
/*
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
*/
         $sql='ALTER TABLE clientes
                MODIFY id int(6) NOT NULL AUTO_INCREMENT;';
            $crear_tb_crearindices = $this->miconexion->prepare($sql);
            $crear_tb_crearindices->execute();         
/*
-- AUTO_INCREMENT de la tabla `presupuestocabecera`
*/
            $sql='ALTER TABLE presupuestocabecera
                MODIFY id int(6) NOT NULL AUTO_INCREMENT;';
            $crear_tb_crearindices = $this->miconexion->prepare($sql);
            $crear_tb_crearindices->execute();            
/*
-- AUTO_INCREMENT de la tabla `presupuestolinea`
*/
            $sql='ALTER TABLE presupuestolinea
                MODIFY id int(6) NOT NULL AUTO_INCREMENT;';
            $crear_tb_crearindices = $this->miconexion->prepare($sql);
            $crear_tb_crearindices->execute();            

            
 /*
-- Restricciones para tablas volcadas

-- Filtros para la tabla `clientes`
*/
              

/*
-- Filtros para la tabla `presupuestocabecera`
*/
            $sql='ALTER TABLE presupuestocabecera
                    ADD CONSTRAINT presupuestocabecera_ibfk_1 FOREIGN KEY (codigocliente) REFERENCES clientes (id) ON DELETE CASCADE ON UPDATE CASCADE;';
            $crear_tb_crearindices = $this->miconexion->prepare($sql);
            $crear_tb_crearindices->execute();       
            
            $sql='ALTER TABLE presupuestolinea
                    ADD CONSTRAINT presupuestolinea_ibfk_1 FOREIGN KEY (idCabecera) REFERENCES presupuestocabecera (id) ON DELETE CASCADE ON UPDATE CASCADE;';
            $crear_tb_crearindices = $this->miconexion->prepare($sql);
            $crear_tb_crearindices->execute();    
       
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        // NUEVO NO PROBADO - TABLAS NUEVAS 
        
          $sql='CREATE TABLE IF NOT EXISTS TiposCaja (
                  Id INT NOT NULL AUTO_INCREMENT , 
                  Descripcion VARCHAR(250) NOT NULL , 
                  Abreviatura VARCHAR(10) NOT NULL , 
                  UNIQUE IdTiposCajas (Id)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_spanish_ci;';
            $crear_tb_presupuestolinea = $this->miconexion->prepare($sql);
            $crear_tb_presupuestolinea->execute();
             echo "Creada tabla: Tipos de Cajas <br>";
        
            $sql='CREATE TABLE IF NOT EXISTS Calidades (
                    Calidad INT(6) NOT NULL , 
                    Descripcion VARCHAR(200) NOT NULL , 
                    Abreviatura VARCHAR(6) NOT NULL , 
                    PRIMARY KEY (Calidad), 
                    INDEX (Abreviatura)) ENGINE = InnoDB;CHARACTER SET utf8 COLLATE utf8_spanish_ci;';
            $crear_tb_presupuestolinea = $this->miconexion->prepare($sql);
            $crear_tb_presupuestolinea->execute();
             echo "Creada tabla: Tipos de Calidades <br>";
        
        
          $sql='CREATE TABLE IF NOT EXISTS articulos (
            Codigo INT(6) NOT NULL , 
            CodigoCliente INT(6) NOT NULL  
            TipoCaja INT(6) NOT NULL , 
            Descripcion VARCHAR(250) NOT NULL , 
            largo DOUBLE NOT NULL , 
            ancho DOUBLE NOT NULL , 
            alto DOUBLE NOT NULL , 
            Calidad INT(6) NOT NULL ,
            PRIMARY KEY (Codigo), INDEX TipoCaja (TipoCaja), 
            UNIQUE IdCliente (CodigoCliente)
            INDEX Calidad (Calidad)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_spanish_ci;';
            $crear_tb_presupuestolinea = $this->miconexion->prepare($sql);
            $crear_tb_presupuestolinea->execute();
             echo "Creada tabla: Articulos<br>";
      
        $this->miconexion = false;
    }

   /* public function getRepresentantes() {
            $bd=$us=$pw="";
            $bd=Config::$bdnombre;
            $us=Config::$bdusuario;
            $pw=Config::$bdclave;
    try  {
            $mdb = conectaDB($bd,$us,$pw); 
            $CsqlRespuestas = "select * from representantes";
            $resultado=$mdb->query($CsqlRespuestas);
            //echo ($resultado);  
            // Añado esto que lo tenia en representantesMenu para devolver un array de objetos representantes
            $representantes[] = new Representantes("", "", "");
            $count=0;
             while ($linea = $resultado->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                    // $datos = $fila[0] . "\t" . $fila[1] . "\t" . $fila[2] . "\n";
                     $id=$nom=$apell="";
                     $id=$linea[0];
                     $nom=$linea[1];
                     $apell=$linea[2];
                     $representantes[$count] = new Representantes("", "", "");
                     $representantes[$count]->setId($id);
                     $representantes[$count]->setNombre($nom);
                     $representantes[$count]->setApellidos($apell);
                     
                     $count++;
        
             }
            // Hasta aqui
            return $representantes;
          
    } catch (Exception $e ) {
        echo $e->getMessage();
    }
    
    }
*/
    public function ultimoCliente() {
         /*   $bd=$us=$pw="";
            $bd=Config::$bdnombre;
            $us=Config::$bdusuario;
            $pw=Config::$bdclave;*/
    try  {
          //  $mdb = conectaDB($bd,$us,$pw); 
            $CsqlRespuestas = $this->miconexion->prepare("select * from clientes");
            $CsqlRespuestas->execute();
           // $resultado=$mdb->query($CsqlRespuestas);
            $UltimoCliente=0;
            $count=0;
             while ($linea = $CsqlRespuestas->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                     // $datos = $fila[0] . "\t" . $fila[1] . "\t" . $fila[2] . "\n";
                     $id=0;
                     $id=$linea[0];
                     if ($id>$UltimoCliente){
                         $UltimoCliente=$id;
                     }
               }
          return $UltimoCliente+1;
      
    } catch (Exception $e ) {
        echo $e->getMessage();
    }
    
    }
   

    public function ultimoRepresetante() {
        /*   $bd=$us=$pw="";
            $bd=Config::$bdnombre;
            $us=Config::$bdusuario;
            $pw=Config::$bdclave;*/
    try  {
         //   $mdb = conectaDB($bd,$us,$pw); 
            $CsqlRespuestas = $this->miconexion->prepare("select * from representantes");
            $resultado=$mdb->query($CsqlRespuestas);
            $UltimoRepresentante=0;
            $count=0;
             while ($linea = $resultado->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                     // $datos = $fila[0] . "\t" . $fila[1] . "\t" . $fila[2] . "\n";
                     $id=0;
                     $id=$linea[0];
                     if ($id>$UltimoRepresentante){
                         $UltimoRepresentante=$id;
                     }
               }
          return $UltimoRepresentante+1;
      
    } catch (Exception $e ) {
        echo $e->getMessage();
    }
    
    }
    public function validar($dato){
        $data=trim($dato); // quitamos los espacioes en blanco por delante y por detas.
        $data=stripslashes($data); // quitamos los slash para evitar que nos lancen un comando oculto entre los datos
        $data=  strip_tags($data);
        $data=htmlspecialchars($data); // Convierto los caracteres especiales de html
        return $data;
    }
   public function datosRepresValidos($nom,$apell) {
       $valido =$valido1=$valido2=false;
       
        $valido1=is_string($nom) ? true : false;
        $valido2= is_string($apell) ? true : false;
        
        return ($valido1 && $valido2);
    }
   public function datosClientValidos($razonsocial,$direccion,$idRep){
       $valido =$valido1=$valido2=$valido3=$valido4=$valido5=$valido6=$valido7=false;
       $valido1 = is_string($razonsocial) ? true : false;
       $valido2 = is_string($direccion)? true : false;
       $valido3 = is_string($poblacion)? true : false;
       $valido4 = is_string($provincia)? true : false;
       $valido5 = is_string($telefono)? true : false;
       $valido6 = is_string($contacto)? true : false;
       $valido7 = is_numeric($codigopostal)? true : false;
        return ($valido1 && $valido2 && $valido3 && $valido4 && $valido5 && $valido6 && $valido7);
   }
   public function getClienteId($id){
      /*      $bd=$us=$pw="";
            $bd=Config::$bdnombre;
            $us=Config::$bdusuario;
            $pw=Config::$bdclave; */
            $razonsocial=$direccion=$poblacion=$provincia=$codigopostal=$telefono=$contacto="";
            $cliDevuelto = [];
            $idBusqueda=$id;
        try{
            ////NUEVO
         //   $mdb = conectaDB($bd,$us,$pw); 
            if (trim($id=="")){
                $id=0;
                $idBusqueda=0;
            }
           // $CsqlRespuestas ="select * from clientes where id ='".$idBusqueda."'";
           // $resultado=$mdb->query($CsqlRespuestas);
            
            $CsqlRespuestas = $this->miconexion->prepare("select * from clientes where id =:id");
            $CsqlRespuestas->bindParam(':id',$id);
            $CsqlRespuestas->execute();
            //$CsqlRespuestas->execute(array($_GET['id']));
   
           $count=0;
            while ($linea = $CsqlRespuestas->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){ //fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT) -->
                     $id=$linea[0];
                     $razonsocial=$linea[1];
                     $direccion=$linea[2];
                     $poblacion=$linea[3];
                     $provincia=$linea[4];
                     $codigopostal=$linea[5];
                     $telefono=$linea[6];
                     $contacto=$linea[7];
                     $cliDevuelto[$count] = new Clientes("", "", "","","","","","");
                     
                    $cliDevuelto[$count]->setId($id); 
                    $cliDevuelto[$count]->setRazonSocial($razonsocial);
                    $cliDevuelto[$count]->setDireccion($direccion);
                    $cliDevuelto[$count]->setPoblacion($poblacion);
                    $cliDevuelto[$count]->setProvincia($provincia);
                    $cliDevuelto[$count]->setCodigoPostal($codigopostal);
                    $cliDevuelto[$count]->setTelefono($telefono);
                    $cliDevuelto[$count]->setContacto($contacto);
                     
                   //  $repreDevuelto[0]=[$id,$nom,$apell];
                     $count++;
                     
             }
             //   print "<br>Modelo MySql Count GetRepresentantes(id) -> ".$count."<br><br>";
             if ($count == 0){
                        $cliDevuelto[$count] = new Clientes("", "", "","","","","","");
                        $cliDevuelto[$count]->setId($id); 
                        $cliDevuelto[$count]->setRazonSocial($razonsocial);
                        $cliDevuelto[$count]->setDireccion($direccion);
                        $cliDevuelto[$count]->setPoblacion($poblacion);
                        $cliDevuelto[$count]->setProvincia($provincia);
                        $cliDevuelto[$count]->setCodigoPostal($codigopostal);
                        $cliDevuelto[$count]->setTelefono($telefono);
                        $cliDevuelto[$count]->setContacto($contacto);
                }
         return $cliDevuelto;
              //  $repreDevuelto[0];
        
        } catch (Exception $e){
            
        }
     
   }
   public function getTiposCaja(){
       $id=$descrip=$abrev="";
       
       
        $CsqlRespuestas = "select * from tiposcaja";
        $resultado=$this->miconexion->query($CsqlRespuestas);
        $contador=0;
        $tipocaja[] = new TiposCaja("","","");
        while ($linea = $resultado->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
            $id=$linea[0];
            $descrip=$linea[1];
            $abrev=$linea[2];
            $tipocaja[$contador] = new TiposCaja("","","");
            $tipocaja[$contador]->setId($id);
            $tipocaja[$contador]->setDescripcion($descrip);
            $tipocaja[$contador]->setAbreviatura($abrev);
            $contador++;
        }
        return $tipocaja;
   }
   public function getTipoCaja($id){
       $id=$descrip=$abrev="";
        $Csql = $this->miconexion->prepare("Select * from tiposcaja where id=:id");
        $Csql->bindParam(':id',$id);
        $Csql->execute();
        $tipocaja[] = new TiposCaja("","","");
        $contador=0;
        while ($linea = $Csql->fetchAll(PDO::FETCH_NUM,PDO::FETCH_ORI_NEXT)){
            $id=$linea[0];
            $descrip=$linea[1];
            $abrev=$linea[2];
            $tipocaja[$contador] = new TiposCaja("","","");
            $tipocaja[$contador]->setId($id);
            $tipocaja[$contador]->setDescripcion($descrip);
            $tipocaja[$contador]->setAbreviatura($abrev);
            $contador++;
        }
        return $tipocaja;
   }
   public function getCalidades(){
       $calidad=$descrip=$abrev="";
        $CsqlRespuestas = "select * from calidades";
        $resultado=$this->miconexion->query($CsqlRespuestas);
        $contador=0;
        $calidades[] = new Calidades("","","");
        while ($linea = $resultado->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
       
            $calidad=$linea[0];
            $descrip=$linea[1];
            $abrev=$linea[2];
            $calidades[$contador] = new Calidades("","","");
            $calidades[$contador]->setCalidad($calidad);
            $calidades[$contador]->setDescripcion($descrip);
            $calidades[$contador]->setAbreviatura($abrev);
            $contador++;
        }
        
        return $calidades;
   }
   public function getCalidad($id){
        $calidad=$descrip=$abrev="";
        $Csql = $this->miconexion->prepare("Select * from calidades where calidad =:id");
        $Csql->bindParam(':id',$id);
        $Csql->execute();
        $calidades[] = new Calidades("","","");
        $contador=0;
        while ($linea = $Csql->fetchAll(PDO::FETCH_NUM,PDO::FETCH_ORI_NEXT)){
            $calidad=$linea[0];
            $descrip=$linea[1];
            $abrev=$linea[2];
            $calidades[$contador] = new Calidades("","","");
            $calidades[$contador]->setCalidad($id);
            $calidades[$contador]->setDescripcion($descrip);
            $calidades[$contador]->setAbreviatura($abrev);
            $contador++;
        }
        return $calidades;
   }

   public function getCabPrespuesto($id) {
        // Devuelvo un Presupuesto por su Id
        $Csql = $this->miconexion->prepare("Select * from presupuestoCabecera where id=:id");
        $Csql->bindParam(':id',$id);
        $Csql->execute();
        $presupCab[] = new PresupuestoCabecera("","","");
        $contador=0;
        $idlinea="0";
        $fechalinea="01/01/01";
        $idClilinea="0";
        

        while($row = $Csql->fetch(PDO::FETCH_ASSOC)){
        
    //    while ($linea = $Csql->fetchAll(PDO::FETCH_NUM,PDO::FETCH_ORI_NEXT)){
            echo "Nombre: {$row["nombre"]} <br>";
            $idlinea=$row["id"];
            $fechalinea=$row["fecha"] ;                 //$linea[1];
            $idClilinea=$row["codigocliente"]  ;                          //$linea[2];
            $presupCab[$contador]->setId($idlinea);
            $presupCab[$contador]->setFecha($fechalinea);
            $presupCab[$contador]->setIdCli($idClilinea);
            $contador++;
         }
        
         if ($contador == 0){
             $presupCab[$contador]->setId($idlinea);
             $presupCab[$contador]->setFecha($fechalinea);
             $presupCab[$contador]->setIdCli($idClilinea);
         }
      //   $this->miconexion= NULL;
         return $presupCab;
     }

    public function getCabPresupuestos_Cliente($idCliente) {
        // Devuelvo Todos los presupuestos de Un cliente
        // Cuando haga click en el presupuesto, me traeré el presupuesto x su id 
        
        $Csql = $this->miconexion->prepare("Select * from presupuestoCabecera where codigocliente= :idCli");
        $Csql->bindParam(':idCli',$idCliente);
        $Csql->execute();
        $presupCab[] = new PresupuestoCabecera("","","");
        $contador=0;
        while ($linea = $Csql->fetch(PDO::FETCH_NUM,PDO::FETCH_ORI_NEXT)){
            $presupCab[$contador] = new PresupuestoCabecera("","","");
            $idlinea=$linea[0];
            $fechalinea=$linea[2];
            $idClilinea=$linea[1];
            $presupCab[$contador]->setId($idlinea);
            $presupCab[$contador]->setFecha($fechalinea);
            $presupCab[$contador]->setIdCli($idClilinea);
            $contador++;
         }
         if ($contador == 0){
             $presupCab[$contador]->setId(0);
             $presupCab[$contador]->setFecha('01/01/01');
             $presupCab[$contador]->setIdCli(0);
         }
    //     $this->miconexion= NULL;
         return $presupCab;
        
        
    }
    
    public function getVisitasCliente($id){
        $Csql = $this->miconexion->prepare("Select * from visitasCliente where codigocliente= :idCli");
        $Csql->bindParam(':idCli',$id);
        $Csql->execute();
        $vistasCliente[] = new VisitasCliente("","","","","");
        $contador=0;
        while ($linea = $Csql->fetch(PDO::FETCH_NUM,PDO::FETCH_ORI_NEXT)){
            $vistasCliente[] = new VisitasCliente("","","","","");
            $id=$linea[0];
            $fecha=$linea[1];
            $codigoCliente=$linea[2];
            $texto=$linea[3];
            $estado=$linea[4];
            $vistasCliente[$contador]->setId($id);
            $vistasCliente[$contador]->setFecha($fecha);
            $vistasCliente[$contador]->setCodigoCliente($codigoCliente);
            $vistasCliente[$contador]->setTexto($texto);
            $vistasCliente[$contador]->setEstado($estado);
            $contador++;
        }
        if ($contador == 0){
            $vistasCliente[$contador]->setId(0);
            $vistasCliente[$contador]->setFecha('01/01/01');
            $vistasCliente[$contador]->setCodigoCliente(0);
            $vistasCliente[$contador]->setTexto('');
            $vistasCliente[$contador]->setEstado('');
        }
        return $vistasCliente;
    }
    
    public function getArticulosCliente($id){
        $codigo=$codigocliente=$tipocaja=$descripcion=$largo=$ancho=$alto=$calidad=$precio="0";
        $Csql = $this->miconexion->prepare("Select * from articulos where codigocliente= :idCli");
        $Csql->bindParam(':idCli',$id);
        $Csql->execute();
        $articCliente[] = new Articulos("","","","","","","","","");
        $contador=0;
        while ($linea = $Csql->fetch(PDO::FETCH_NUM,PDO::FETCH_ORI_NEXT)){
            $articCliente[$contador] = new Articulos("","","","","","","","","");
            $codigo=$linea[0];
            $codigocliente=$linea[1];
            $tipocaja=$linea[2];
            $descripcion=$linea[3];
            $largo=$linea[4];
            $ancho=$linea[5];
            $alto=$linea[6];
            $calidad=$linea[7];
            $precio=$linea[8];
            $articCliente[$contador]->setcodigo($codigo);
            $articCliente[$contador]->setcodigocliente($codigocliente);
            $articCliente[$contador]->settipocaja($tipocaja);
            $articCliente[$contador]->setdescripcion($descripcion);
            $articCliente[$contador]->setlargo($largo);
            $articCliente[$contador]->setancho($ancho);
            $articCliente[$contador]->setalto($alto);
            $articCliente[$contador]->setcalidad($calidad);
            $articCliente[$contador]->setprecio($precio);
            $contador++;
        }
            if ($contador == 0){
                $articCliente[$contador]->setcodigo($codigo);
                $articCliente[$contador]->setcodigocliente($codigocliente);
                $articCliente[$contador]->settipocaja($tipocaja);
                $articCliente[$contador]->setdescripcion($descripcion);
                $articCliente[$contador]->setlargo($largo);
                $articCliente[$contador]->setancho($ancho);
                $articCliente[$contador]->setalto($alto);
                $articCliente[$contador]->setcalidad($calidad);
                $articCliente[$contador]->setprecio($precio);
            }
            return $articCliente;
    }
    public function getArticulo($id){
        $codigo=$codigocliente=$tipocaja=$descripcion=$largo=$ancho=$alto=$calidad=$precio="0";
        $Csql = $this->miconexion->prepare("Select * from articulos where codigo= :idArt");
        $Csql->bindParam(':idArt',$id);
        $Csql->execute();
        $articulo[] = new Articulos("","","","","","","","","");
       
        $contador=0;
        while ($linea = $Csql->fetch(PDO::FETCH_NUM,PDO::FETCH_ORI_NEXT)){
            $articulo[$contador] = new Articulos("","","","","","","","","");
            $codigo=$linea[0];
            $codigocliente=$linea[1];
            $tipocaja=$linea[2];
            $descripcion=$linea[3];
            $largo=$linea[4];
            $ancho=$linea[5];
            $alto=$linea[6];
            $calidad=$linea[7];
            $precio=$linea[8];
            $articulo[$contador]->setcodigo($codigo);
            $articulo[$contador]->setcodigocliente($codigocliente);
            $articulo[$contador]->settipocaja($tipocaja);
            $articulo[$contador]->setdescripcion($descripcion);
            $articulo[$contador]->setlargo($largo);
            $articulo[$contador]->setancho($ancho);
            $articulo[$contador]->setalto($alto);
            $articulo[$contador]->setcalidad($calidad);
            $articulo[$contador]->setprecio($precio);
           
            $contador++;
        }
            if ($contador == 0){
                $articulo[$contador]->setcodigo($codigo);
                $articulo[$contador]->setcodigocliente($codigocliente);
                $articulo[$contador]->settipocaja($tipocaja);
                $articulo[$contador]->setdescripcion($descripcion);
                $articulo[$contador]->setlargo($largo);
                $articulo[$contador]->setancho($ancho);
                $articulo[$contador]->setalto($alto);
                $articulo[$contador]->setcalidad($calidad);
                $articulo[$contador]->setprecio($precio);
            }
            return $articulo;
    
    }
    public function getimagenArticulo($codArticulo){
            $imagen="";
            $imagenArticulo='D:/BasesDeDatos/LaPlana/Imagenes/' . $codArticulo.".JPG" ;
            $imagenArticulo='../app/templates/imagenes/' . $codArticulo . '.jpg' ;
  
            if (file_exists($imagenArticulo)){
            $imagen=$imagenArticulo;
            }
        return $imagen;
    }
    public function getCabeceraPresupuesto($idCabeceraPresupuesto){
         $Csql = $this->miconexion->prepare("Select * from presupuestoCabecera where id= :idCab");
        $Csql->bindParam(':idCab',$idCabeceraPresupuesto);
        $Csql->execute();
        $presupCab[] = new PresupuestoCabecera("","","");
        $contador=0;
        while ($linea = $Csql->fetch(PDO::FETCH_NUM,PDO::FETCH_ORI_NEXT)){
            $presupCab[$contador] = new PresupuestoCabecera("","","");
            $idlinea=$linea[0];
            $fechalinea=$linea[2];
            $idClilinea=$linea[1];
            $presupCab[$contador]->setId($idlinea);
            $presupCab[$contador]->setFecha($fechalinea);
            $presupCab[$contador]->setIdCli($idClilinea);
            $contador++;
         }
         if ($contador == 0){
             $presupCab[$contador]->setId(0);
             $presupCab[$contador]->setFecha('01/01/01');
             $presupCab[$contador]->setIdCli(0);
         }
    //     $this->miconexion= NULL;
         return $presupCab;
     
        
        
        
    }

    public function getDetallePresupuesto($idCab) {
        // Saco el detalle de un presupuesto segun la Id de la cabecera
        $idCabecera=$idCab;
        //$Csql = $this->miconexion;
        //$CsqlRespuestas = ("Select * from presupuestolinea where idCabecera=$idCab");
        //$conecta=$modelo->miconexion;
        // $resultado=$conecta->query($CsqlRespuestas);
             
         
        $Csql=$this->miconexion->prepare("Select * from presupuestolinea where idCabecera=$idCab");
      // $Csql->bindParam(':idCab',$idCabecera);
       $Csql->execute();
        $Detalle[] = new PresupuestoDetalle("","","","","","","");
        $contador=0;
        while ($linea = $Csql->fetch(PDO::FETCH_NUM,PDO::FETCH_ORI_NEXT)){
            $Detalle[$contador] = new PresupuestoDetalle("","","","","","");
            $id=$linea[0];
            $idCabecera=$linea[1];
            $lineadet=$linea[2];
            $codigo=$linea[3];
            $descripcion=$linea[4];
            $escalado=$linea[5];
            $precio=$linea[6];
            $Detalle[$contador]->setId($id);
            $Detalle[$contador]->setIdCabecera($idCabecera);
            $Detalle[$contador]->setLinea($lineadet);
            $Detalle[$contador]->setCodigo($codigo);
            $Detalle[$contador]->setDescripcion($descripcion);
            $Detalle[$contador]->setEscalado($escalado);
            $Detalle[$contador]->setPrecio($precio);
            $contador++;
            }
            if ($contador == 0){
            $Detalle[$contador]->setId(0);
            $Detalle[$contador]->setIdCabecera(0);
            $Detalle[$contador]->setLinea(0);
            $Detalle[$contador]->setCodigo(0);
            $Detalle[$contador]->setDescripcion('');
            $Detalle[$contador]->setEscalado('');
            $Detalle[$contador]->setPrecio(0);
         }
       //     $this->miconexion= NULL;
            return $Detalle;
    }
    function inicializaTemporal(){
    $Csql="Delete  from presupuestolineatmp";
    
    $resultado=$this->miconexion->query($Csql);
  }
  function setPresupuestos($Cabecera,$Detalle){
            $id=$Cabecera->getid();
            $fecha=$Cabecera->getfecha();
            $codigocliente=$Cabecera->getidCli()  ;
            $Csql=$this->miconexion->prepare("INSERT INTO presupuestocabecera (id,fecha,codigocliente) VALUES (:id,:fecha,:codigocliente)");
            $Csql->bindParam(':id', $id);     
            $Csql->bindParam(':fecha', $fecha);  
            $Csql->bindParam(':codigocliente', $codigocliente);  
            $Csql->execute();
            $j=count($Detalle)-1;
            $siguiente= sacaultimo($this->miconexion, "id",  "presupuestolinea");
            $Csql=$this->miconexion->prepare("INSERT INTO presupuestolinea (id,idCabecera,linea,codigo,descripcion,escalado,precio) VALUES (:id,:idCabecera,:linea,:codigo,:descripcion,:escalado,:precio)");
            for ($li=0;$li<$j;$li++){
                $estedetalle=$Detalle[$li];
                $id=$siguiente;
                $idCabecera=$estedetalle->getIdCabecera();
                $linea=$estedetalle->getlinea();
                $codigo=$estedetalle->getcodigo();
                $descripcion=$estedetalle->getdescripcion();
                $escalado=$estedetalle->getescalado();
                $precio=$estedetalle->getprecio();
              if (count_chars(trim($codigo))>0) {    
                $Csql->bindParam(':id', $id);     
                $Csql->bindParam(':idCabecera', $idCabecera);  
                $Csql->bindParam(':linea', $linea);  
                $Csql->bindParam(':codigo', $codigo);  
                $Csql->bindParam(':descripcion', $descripcion); 
                $Csql->bindParam(':escalado', $escalado); 
                $Csql->bindParam(':precio', $precio);  
                $Csql->execute();
                $lineadetalle[$li]=new PresupuestoDetalle("","","","","","","");
                $lineadetalle[$li]->setId($id);
                $lineadetalle[$li]->setIdCabecera($idCabecera);
                $lineadetalle[$li]->setLinea($linea);
                $lineadetalle[$li]->setCodigo($codigo);
                $lineadetalle[$li]->setDescripcion($descripcion);
                $lineadetalle[$li]->setEscalado($escalado);
                $lineadetalle[$li]->setPrecio($precio);
         //   return $lineadetalle;
                $siguiente++;
              }
            }
  }
 }
